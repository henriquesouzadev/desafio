import styled from 'styled-components'

export const AreaHeader = styled.header`
   .top-bar {
      display: flex;
      
      background-color: var(--color-black-700);
      color: #fff;
      padding: 8px 0;

      .wrap {
         justify-content: flex-end;

         a {
            font-weight: 700;
            text-decoration: underline;
            color: #fff;
         }
      }
   }

   .main-bar {
      margin: 2rem 0;

      .wrap {
         justify-content: space-between;
         align-items: center;

         .menu-btn { display: none; }

         .logo { width: 180px; }

         .search {
            display: flex;

            input {
               width: 350px;
               padding: .8rem 1.5rem;
               border: 1px solid var(--color-gray-600);
            }

            button {
               font-weight: 700;
               font-size: 14px;
               text-transform: uppercase;
               color: #fff;
               background: var(--color-red-400);
               padding: .8rem 1.5rem;
            }
         }

         .search-mobile { display: none; }
      }
   }


   /* RESPONSIVE */
   @media (max-width: 768px) {

      .top-bar {
         .wrap {
            justify-content: center;
         }
      }

      .main-bar {
         .wrap {
            .menu-btn {
               display: block;

               button {
                  background: transparent;

                  span {
                     display: flex;
                     width: 24px;
                     height: 4px;
                     background: #ce0c24;
                     margin-bottom: 4px;

                     &:last-of-type { margin-bottom: 0; }
                  }
               }  
            }

            .search { display: none; }

            .search-mobile {
               display: block;

               button { 
                  display: flex;
                  width: 30px;
                  height: 30px;
                  background: transparent;

                  svg {
                     fill: #ce0c24;
                  }
               }
            }
         }
      }
      
   }
`