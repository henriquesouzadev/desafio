import React from 'react'
import { AreaHeader } from './styled'
import logo from './../../assets/logo.png'
import { ReactComponent as Search } from './../../assets/search.svg'
import MenuCategories from './MenuCategories'

const Header = () => {

   const [menuActive, setMenuActive] = React.useState(false);

   return (
      <AreaHeader>
         <div className="top-bar">
            <div className="container">
               <div className="wrap">
                  <div className="links">
                     <a href="/#">Acesse sua conta</a> ou <a href="/#">Cadastre-se</a>
                  </div>
               </div>
            </div>
         </div>

         <div className="main-bar">
            <div className="container">
               <div className="wrap">

                  <div className="menu-btn">
                     <button onClick={() => setMenuActive(!menuActive)}>
                        <span></span>
                        <span></span>
                        <span></span>
                     </button>
                  </div>

                  <div className="logo">
                     <img src={logo} alt="Logo Webjump" className="img-fluid" />
                  </div>

                  <div className="search">
                     <input type="text" />
                     <button type="button">Buscar</button>
                  </div>

                  <div className="search-mobile">
                     <button>
                        <Search />
                     </button>
                  </div>

               </div>
            </div>
         </div>

         <MenuCategories menuActive={menuActive} />
      </AreaHeader >
   )
}

export default Header
