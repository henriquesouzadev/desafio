import React from 'react'
import { Link } from 'react-router-dom'
import { AreaMenuCategories } from './styled'

const MenuCategories = ({ menuActive }) => {

   console.log(menuActive)

   const [categories, setCategories] = React.useState([]);

   React.useEffect(() => {
      async function fetchData() {
         const response = await fetch('/api/list.json');
         const json = await response.json();
         setCategories(json.items);
      }
      fetchData();
   }, [])

   return (
      <AreaMenuCategories
         display={menuActive ? 'block' : 'none'}
      >
         < div className="container" >
            <ul>
               <li>
                  <Link to="/">Página inicial</Link>
               </li>
               {categories.length > 0 && categories.map(({ id, name, path }) => (
                  <li key={id}>
                     <Link to={`/categoria/${id}`}>{name}</Link>
                  </li>
               ))}
               <li>
                  <Link to="/">Contato</Link>
               </li>
            </ul>
         </div >
      </AreaMenuCategories >
   );
}

export default MenuCategories
