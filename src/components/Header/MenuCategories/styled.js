import styled from 'styled-components'

export const AreaMenuCategories = styled.div`
   background: var(--color-red-400);

   ul {
      display: flex;
      padding: 1.2rem 0;

      li {
         padding-right: 4rem;

         a {
            font-weight: 700;
            font-size: 1rem;
            text-transform: uppercase;
            color: #fff;
         }
      }
   }


   /* RESPONSIVE */
   @media (max-width: 768px) {
      display: ${props => props.display};

      ul {
         flex-direction: column;

         li {
            margin-top: 4px;

            a {
               font-weight: 400;
               font-size: .9rem;
            }
         }
      }
   }
`