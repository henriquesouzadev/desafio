import styled from 'styled-components'

export const AreaCategory = styled.div`
   .breadcrumb {
      display: block;

      ul {
         display: flex;
         padding: 1.5rem 0;

         li {
            font-weight: 500;

            a { color: #000; }

            &::after {
               content: ">";
               color: var(--color-gray-400);
               margin: 0 .4rem;
            }

            &:last-of-type::after { display: none; }
         }
      }
   }

   .main {
      .wrap {
         display: flex;
         gap: 30px;

         .content {
            flex: 1;

            .title {
               font-weight: 400;
               font-size: 2rem;
               color: var(--color-red-400);
            }

            .ordering-bar {
               display: flex;
               justify-content: flex-end;
               margin-top: 1rem;
               padding: .7rem 0;
               border-top: 1px solid var(--color-gray-300);
               border-bottom: 1px solid var(--color-gray-300);

               .field-select {
                  display: flex;
                  align-items: center;
                  font-weight: 600;
                  font-size: .8rem;
                  color: var(--color-gray-600);
                  text-transform: uppercase;

                  select {
                     width: 220px;
                     margin-left: 1rem;
                     padding: 4px 10px;
                     border: 1px solid var(--color-gray-400);
                     border-radius: 4px;
                  }
               }
            }

            .list {
               display: grid;
               grid-template-columns: repeat(4, 1fr);
               width: 100%;
               gap: 1.5rem;
               margin-top: 1.5rem;
            }
         }
      }
   }


   /* RESPONSIVE */
   @media (max-width: 1024px) {
      .main {
         .wrap {
            .content {
               .list { grid-template-columns: repeat(3, 1fr); }
            }
         }
      }
   }

   @media (max-width: 768px) {
      .main {
         .wrap {
            flex-direction: column;

            .content {
               width: 100%;

               .ordering-bar {
                  .field-select {
                     font-size: .7rem;

                     select { width: 180px; }
                  }
               }

               .list { grid-template-columns: repeat(2, 1fr); }
            }
         }
      }
   }
`