import React from 'react'
import { Item } from './styled'

const Product = (props) => {
   return (
      <Item>
         <div className="cover">
            <img
               src={`${process.env.PUBLIC_URL}/${props.image}`}
               alt={props.name}
            />
         </div>
         <div className="data">
            <div className="top">
               <div className="name">{props.name}</div>
            </div>

            <div className="bottom">
               <div className="price">
                  <div className={`normal ${props.specialPrice ? 'off' : ''}`}>
                     {props.price.toLocaleString('pt-BR', {
                        style: 'currency',
                        currency: 'BRL'
                     })}
                  </div>

                  {props.specialPrice && (
                     <div className="special">
                        {props.specialPrice.toLocaleString('pt-BR', {
                           style: 'currency',
                           currency: 'BRL'
                        })}
                     </div>
                  )}
               </div>

               <button>Comprar</button>
            </div>
         </div>
      </Item>
   )
}

export default Product
