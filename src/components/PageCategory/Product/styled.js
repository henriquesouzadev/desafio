import styled from 'styled-components'

export const Item = styled.li`
   display: flex;
   flex-direction: column;

   .cover {
      padding: 1.5rem 0;
      border: 1px solid var(--color-gray-300);

      img {
         width: 100%;
         height: 160px;
         object-fit: contain;
      }
   }

   .data {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      height: 100%;
      text-align: center;
      padding: .6rem 0;

      .top {
         .name {
            font-size: 1rem;
            text-transform: uppercase;
            color: var(--color-gray-600);
         }
      }

      .bottom {
         display: flex;
         flex-direction: column;

         .price {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 100%;
            font-weight: 800;
            font-size: 1.4rem;
            letter-spacing: -1px;
            color: var(--color-red-400);
            margin-top: .5rem;

            .normal {
               &.off {
                  font-weight: 400;
                  font-size: 1rem;
                  text-decoration: line-through;
                  color: var(--color-gray-600);
                  margin-right: .7rem;
               }
            }
         }

         button {
            font-weight: 800;
            font-size: 1rem;
            text-transform: uppercase;
            color: #fff;
            background: var(--color-gray-400);
            margin-top: .5rem;
            padding: .5rem 1rem;
            border-radius: 5px;
         }
      }
   }


   /* RESPONSIVE */
   .cover { img { height: 120px; } }

   .bottom {
      .price {
         display: flex;
         flex-direction: column;
      }
   } 
`