import styled from 'styled-components'

export const AreaFilters = styled.div`
   .filters {
      width: 250px;
   
      .box {
         padding: 1rem;
         border: 1px solid #ccc;

         .title {
            font-weight: 700;
            font-size: 1.5rem;
            text-transform: uppercase;
            color: var(--color-red-400);
         }
         
         .type {
            margin-top: 1.3rem;

            .subtitle {
               font-weight: 700;
               font-size: 1.1rem;
               text-transform: uppercase;
               color: var(--color-gray-600);
            }

            ul {
               li {
                  font-size: 1rem;
                  color: var(--color-gray-900);
                  margin-top: .5rem;

                  a { color: var(--color-gray-900); }
               }

               &.colors {
                  display: flex;
                  gap: 4px;

                  li {
                     width: 60px;

                     button {
                        display: block;
                        width: 100%;
                        height: 30px;
                        background: #ccc;
                     }
                  }
               }
            }
         }
      }
   }


   /* RESPONSIVE */
   @media (max-width: 768px) {
      .filters {
         width: 100%;

         .box {
            .type {
               ul {
                  li { font-weight: 400; }
               }
            }
         }
      }
   }
`