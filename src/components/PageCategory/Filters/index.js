import React from 'react'
import { AreaFilters } from './styled'
import { Link } from 'react-router-dom';

const Filters = () => {
   return (
      <AreaFilters>
         <div className="filters">

            <div className="box">
               <h2 className="title">Filtre por</h2>

               <div className="type">
                  <h3 className="subtitle">Categorias</h3>
                  <ul>
                     <li>
                        <Link to="/categoria/1">Camisetas</Link>
                     </li>
                     <li>
                        <Link to="/categoria/2">Calças</Link>
                     </li>
                     <li>
                        <Link to="/categoria/3">Calçados</Link>
                     </li>
                  </ul>
               </div>

               <div className="type">
                  <h3 className="subtitle">Cores</h3>
                  <ul className="colors">
                     <li>
                        <button style={{ 'background': '#cc0d1f' }}></button>
                     </li>
                     <li>
                        <button style={{ 'background': '#f36324' }}></button>
                     </li>
                     <li>
                        <button style={{ 'background': '#28a3aa' }}></button>
                     </li>
                  </ul>
               </div>
            </div>

         </div>
      </AreaFilters>
   )
}

export default Filters
