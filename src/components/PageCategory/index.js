import React from 'react';
import { AreaCategory } from './styled';
import { useParams, Link } from 'react-router-dom';
import Product from './Product';
import Filters from './Filters';

const Categoria = () => {

   const { id } = useParams();
   const [category, setCategory] = React.useState({});
   const [products, setProducts] = React.useState([]);

   React.useEffect(() => {
      async function fetchCategory() {
         const response = await fetch('/api/list.json');
         const { items } = await response.json();
         const result = items.filter(item => item.id === parseInt(id));
         setCategory(result[0]);
      }
      fetchCategory();
   }, [id]);

   React.useEffect(() => {
      async function fetchProducts() {
         const response = await fetch(`/api/${id}.json`);
         const { items } = await response.json();
         setProducts(items);
      }
      fetchProducts();
   }, [id]);

   return (
      <AreaCategory>
         <div className="breadcrumb">
            <div className="container">
               <ul>
                  <li>
                     <Link to="/">Página inicial</Link>
                  </li>
                  <li>{category.name}</li>
               </ul>
            </div>
         </div>

         <section className="main">
            <div className="container">
               <div className="wrap">

                  <Filters />

                  <div className="content">
                     <h1 className="title">{category.name}</h1>

                     <div className="ordering-bar">
                        <div className="field-select">
                           Ordenar por
                           <select name="order-list" id="order-list">
                              <option value="price">Preço</option>
                           </select>
                        </div>
                     </div>

                     <ul className="list">
                        {products.length > 0 && products.map(({ id, name, image, price, specialPrice }) => (
                           <Product
                              key={id}
                              image={image}
                              name={name}
                              price={price}
                              specialPrice={specialPrice}
                           />
                        ))}
                     </ul>
                  </div>

               </div>
            </div>
         </section>
      </AreaCategory>
   )
}

export default Categoria
