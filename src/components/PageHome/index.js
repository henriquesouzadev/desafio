import React from 'react'
import { AreaHome } from './styled'

const Home = () => {
   return (
      <AreaHome>
         <div className="container">

            <div className="wrap">

               <aside className="menu-left">
                  <ul>
                     <li>
                        <a href="/#">Página inicial</a>
                     </li>
                     <li>
                        <a href="/#">Camisetas</a>
                     </li>
                     <li>
                        <a href="/#">Calças</a>
                     </li>
                     <li>
                        <a href="/#">Sapatos</a>
                     </li>
                     <li>
                        <a href="/#">Contato</a>
                     </li>
                  </ul>
               </aside>

               <section className="content">
                  <div className="banner"></div>
                  <div className="text">
                     <h2 className="title">Seja bem-vindo!</h2>
                     <p>Lorem ipsum dolor sit amet, mea quem putant persecuti ne, id quo vero adversarium. Sea eleifend molestiae ex. Ut cum doctus sanctus officiis, nec dolore signiferumque ne. Duo at aperiam salutatus, nam partiendo consequat ea, in usu alia brute. Dignissim mediocritatem ius ne. Ei quis fugit nam. Ne cibo offendit oporteat quo, quo alia discere te. No mei idque inimicus. Et prompta deleniti petentium mei, offendit officiis eos in. Mea nostrud phaedrum no, falli electram intellegebat no has. At sea feugiat molestie. Eam enim civibus placerat ut, mucius graecis hendrerit ea qui. Et ridens commodo malorum eam, ex eam offendit percipit pertinax. Vim ex constituam ullamcorper, ea sed fabulas omittantur, his inani splendide an. Pri labore labitur meliore ea, sed ea quem dico stet. No mei idque inimicus. Et prompta deleniti petentium mei, offendit officiis eos in. Mea nostrud phaedrum no, falli electram intellegebat no has. At sea feugiat molestie. Eam enim civibus placerat ut, mucius graecis hendrerit ea qui. Et ridens commodo malorum eam, ex eam offendit percipit pertinax. Vim ex constituam ullamcorper, ea sed fabulas omittantur, his inani splendide an. Pri labore labitur meliore ea, sed ea quem dico stet.</p>
                  </div>
               </section>

            </div>

         </div>
      </AreaHome>
   )
}

export default Home
