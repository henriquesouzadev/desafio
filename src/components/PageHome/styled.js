import styled from 'styled-components'

export const AreaHome = styled.main`
   display: flex;
   margin-top: 1rem;

   .wrap {
      display: flex;
      gap: 1rem;

      .menu-left {
         width: 210px;
         background: var(--color-gray-300);
         padding: 1.5rem;

         ul {
            li {
               margin-bottom: 1rem;

               a {
                  font-weight: 500;
                  font-size: 1.2rem;
                  color: var(--color-gray-900);
               }
            }
         }
      }

      .content {
      flex: 1;

         .banner {
            width: 100%;
            height: 220px;
            background: var(--color-gray-500);
            margin-bottom: 1rem;
         }

         .text {
            .title {
               font-weight: 400;
               font-size: 1.6rem;
               margin-bottom: .6rem;
            }

            p {
               font-size: 1rem;
            }
         }
      }
   }


   /* RESPONSIVE */
   @media (max-width: 768px) {
      .wrap {
         .menu-left { display: none;}

         .content {
            .banner {
               height: 100px;
            }
         }
      }
   }
`