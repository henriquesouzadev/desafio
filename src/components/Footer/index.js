import React from 'react'
import styled from 'styled-components'

const AreaFooter = styled.div`
   width: 100%;
   height: 160px;
   background: var(--color-red-400);
   margin-top: 1rem;
`

const Footer = () => {
   return (
      <footer>
         <div className="container">
            <AreaFooter />
         </div>
      </footer>
   )
}

export default Footer
