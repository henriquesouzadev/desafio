import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';
import Header from './components/Header';
import Footer from './components/Footer';
import PageHome from './components/PageHome';
import PageNotFound from './components/PageNotFound';
import PageCategory from './components/PageCategory';

const GlobalStyle = createGlobalStyle`
  :root {
    --color-white: #fff;
    --color-gray-300: #e2dedb;
    --color-gray-400: #80bdb8;
    --color-gray-500: #acacac;
    --color-gray-600: #8a8a8a;
    --color-gray-900: #4e4a47;
    --color-red-400: #cc0d1f;
    --color-black-700: #231f20;
  }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: none;
    border: none;
  }

  body {
    font-family: 'Open Sans', sans-serif;
    font-size: 14px;
  }

  button { cursor: pointer; }

  a { text-decoration: none; }

  ul { list-style: none; }

  .container {
    display: flex;
    width: 1240px;
    margin: 0 auto;
    padding-left: 15px;
    padding-right: 15px;
  }

  .space-between {
    display: flex;
    justify-content: space-between;
  }

  .flex-end {
    display: flex;
    justify-content: flex-end;
  }

  .img-fluid {
    width: 100%;
    height: auto;
  }

  .wrap {
    display: flex;
    width: 100%;
  }


  @media (max-width: 1240px) {
    .container {
      width: 100%;
    }
  }
`;

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/">
            <PageHome />
          </Route>
          <Route path="/categoria/:id">
            <PageCategory />
          </Route>
          <Route path="*">
            <PageNotFound />
          </Route>
        </Switch>
        <Footer />
      </BrowserRouter>
    </div >
  );
}

export default App;
